from bson.objectid import ObjectId
from typing import List
from .client import Queries
from models import BookIn, BookOut, Loan, LoanIn


class BookQueries(Queries):
    DB_NAME = "library"
    COLLECTION = "inventory"

    def create(self, book: BookIn) -> BookOut:
        props = book.dict()
        self.collection.insert_one(props)
        props["id"] = str(props["_id"])
        return BookOut(loans=[], **props)

    def get_all(self) -> List[BookOut]:
        result = self.collection.aggregate(
            [
                {
                    "$lookup": {
                        "from": "loans",
                        "localField": "_id",
                        "foreignField": "book_id",
                        "as": "loans",
                    }
                },
                {"$sort": {"title": 1}},
            ]
        )
        bookPropsList = list(result)
        for bookProps in bookPropsList:
            bookProps["id"] = str(bookProps["_id"])
            bookProps["loans"] = [
                str(props["account_id"]) for props in bookProps["loans"]
            ]
        return [BookOut(**book) for book in bookPropsList]


class LoanQueries(Queries):
    DB_NAME = "library"
    COLLECTION = "loans"

    def create(self, loan: LoanIn) -> Loan:
        props = loan.dict()
        props["account_id"] = ObjectId(props["account_id"])
        props["book_id"] = ObjectId(props["book_id"])
        self.collection.insert_one(props)
        props["id"] = str(props["_id"])
        props["account_id"] = str(props["account_id"])
        props["book_id"] = str(props["book_id"])
        return Loan(**props)

    def delete(self, book_id: str, account_id: str):
        self.collection.delete_one(
            {
                "account_id": ObjectId(account_id),
                "book_id": ObjectId(book_id),
            }
        )
