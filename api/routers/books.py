from fastapi import APIRouter, Depends, HTTPException, status
from models import BookIn, BookList, BookOut, LoanIn, LoanOut
from queries.books import BookQueries, LoanQueries
from routers.sockets import socket_manager
from token_auth import get_current_user

router = APIRouter()

not_authorized = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Invalid authentication credentials",
    headers={"WWW-Authenticate": "Bearer"},
)


@router.post("/books", response_model=BookOut)
async def create_book(
    book: BookIn,
    repo: BookQueries = Depends(),
    account: dict = Depends(get_current_user),    
):
    if "librarian" not in account.roles:
        raise not_authorized
    book = repo.create(book)
    await socket_manager.broadcast_refetch()
    return book


@router.get("/books", response_model=BookList)
def get_books(repo: BookQueries = Depends()):
    return BookList(books=repo.get_all())


@router.post("/books/{book_id}/loans", response_model=LoanOut)
async def create_loan(
    book_id: str,
    repo: LoanQueries = Depends(),
    account: dict = Depends(get_current_user),
):
    if "patron" not in account.roles:
        raise not_authorized
    await socket_manager.broadcast_refetch()
    loan_request = LoanIn(book_id=book_id, account_id=account.id)
    loan_request = repo.create(loan_request)
    return loan_request


@router.delete("/books/{book_id}/loans", response_model=bool)
async def remove_loan(
    book_id: str,
    repo: LoanQueries = Depends(),
    account: dict = Depends(get_current_user),
):
    if "patron" not in account.roles:
        raise not_authorized
    await socket_manager.broadcast_refetch()
    repo.delete(book_id=book_id, account_id=account.id)
    return True
